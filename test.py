import unittest
from comparators import same_number

class TestUtilsComparators(unittest.TestCase):

    def test_same(self):
        self.assertTrue(same_number(1, 1))

    def test_same2(self):
        self.assertTrue(same_number(1, '1'))

    def test_bad_input(self):
        self.assertFalse(same_number(1, 'a'))

if __name__ == '__main__':
    unittest.main()